myGPXsb
=======

My [GPX][gpx] sand box.

What is in it ?
---------------

- A short try with `stdlib` in branch [`stdlib`][stdlib]
- Spiking around [`gpxpy`][gpxpy] in branch [`gpxpy`][gpxpybranch]
- A tool to post data without geolocalized data on Strava's API. Have a look in [`devel`][devel] branch


[devel]:        https://gitlab.com/free_zed/mygpxsb/tree/devel/ "devel branch"
[gpx]:          https://en.wikipedia.org/wiki/GPS_Exchange_Format "GPX on wikipedia"
[gpxpybranch]:  https://gitlab.com/free_zed/mygpxsb/tree/gpxpy/ "gpxpy branch"
[gpxpy]:        https://pypi.org/project/gpxpy/ "GPXPy on pypi.org"
[stdlib]:       https://gitlab.com/free_zed/mygpxsb/tree/stdlib/ "stdlib branch"
